// Homework_16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	
	const int ROW = 7;
	const int COL = 5;
	
	int arr[ROW][COL];
	int sum = 0;
	
	int l = buf.tm_mday % ROW;

	cout << "The number of current day is: " << buf.tm_mday << endl;

	cout << "Now we are building an Array with " << ROW << " rows and " << COL << " columns\n";
	cout << endl;
	
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			arr[i][j] = i+j; 
		}
		
	}

	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			cout << arr[i][j] << "\t";
			if (i==l) sum += arr[i][j];
		}
		cout << endl;
	}
	
	cout << endl;
	cout << "The Sum of Row " << l << " is: " << sum << endl;
			
	
}

